package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

type power struct {
	today   float64 // kWh
	total   int64   // kWh
	current int64   // Watt
}

func getEnv(key, fallback string) string {
	if val, ok := os.LookupEnv(key); ok {
		return val
	}
	return fallback
}

func getVarValue(jsBlob string, key string) string {
	// Figure out the starting index of a value
	s := strings.Index(jsBlob, key+"=") + len(key+"=")
	e := strings.Index(jsBlob[s:], ";") + s

	if s == -1 || e == -1 {
		return ""
	}

	return jsBlob[s:e]
}

func getPower(wd string) power {
	s := strings.Split(wd, ",")
	c, tod, tot := s[5], s[6], s[7]

	p := power{
		total:   convertStringToInt(tot),
		current: convertStringToInt(c),
		today:   convertStringToFloat(tod) / 100,
	}

	return p
}

func convertStringToInt(s string) int64 {
	i, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		panic(err)
	}
	return i
}

func convertStringToFloat(s string) float64 {
	f, err := strconv.ParseFloat(s, 64)
	if err != nil {
		panic(err)
	}
	return f
}

func getStatus() {
	ip := getEnv("INVERTER_IP", "192.168.100.23")
	user := getEnv("INVERTER_USER", "admin")
	password := getEnv("INVERTER_PASS", "admin")

	addr := "http://" + ip + "/js/status.js"

	client := &http.Client{}
	req, err := http.NewRequest("GET", addr, nil)
	if err != nil {
		panic(err)
	}

	req.SetBasicAuth(user, password)

	resp, err := client.Do(req)
	defer resp.Body.Close()

	if resp.StatusCode == http.StatusOK {
		bs, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			panic(err)
		}

		webData := getVarValue(string(bs), "webData")
		p := getPower(webData)
		displayData(p)
	}
}

func displayData(p power) {
	fmt.Println("Current output (Watt): ", p.current)
	fmt.Println("Today's total (kWh): ", p.today)
	fmt.Println("Lifetime total (kWh): ", p.total)
}

func main() {
	for {
		getStatus()
		time.Sleep(3 * time.Second)
	}
}
